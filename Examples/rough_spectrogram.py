import numpy as np
import librosa
from librosa import decompose,display
from PIL import Image
import matplotlib.pyplot as plt

y, sr = librosa.load('blues.00000.au')
spec = librosa.feature.melspectrogram(y, sr, n_mels=128)
print(spec.shape)
plt.plot(spec)
plt.title("Plot of spectrogram as a time series")
plt.show()
im = Image.fromarray(spec)
if im.mode != 'RGB':
    im = im.convert('RGB')
im.save("figure2.png")


librosa.display.specshow(spec, y_axis='log', x_axis='time', cmap='gray')
plt.title("Librosa Spectrogram [Recomendation from piazza]")
plt.show()
im = Image.fromarray(spec)
if im.mode != 'RGB':
    im = im.convert('RGB')
im.save("figure3.png")

'''
S = np.abs(librosa.stft(y, n_fft=2048))
comps, acts = librosa.decompose.decompose(S, n_components=128)
plt.figure(figsize=(10,8))
librosa.display.specshow(librosa.amplitude_to_db(S, ref=np.max), y_axis='log', x_axis='time', cmap='gray')
plt.title("Plot of old spectrogram methodology")
plt.show()
im = Image.fromarray(spec)
if im.mode != 'RGB':
    im = im.convert('RGB')
im.save("figure4.png")

librosa.display.specshow(S, y_axis='log', x_axis='time', cmap='gray')
plt.title("Plot of old spectrogram methodology without amplitude to db")
plt.show()
'''
