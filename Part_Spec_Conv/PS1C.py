#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Christopher Zapotocky and Francisco Viramontes
"""
#These modules are for building the data
import os
import librosa
import numpy as np
import time

#Module for applying the haar wavelet transform
import pywt

import matplotlib.pyplot as plt

#These modules are for running the Neural Network
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Conv1D
from keras.optimizers import SGD
from keras import regularizers
from keras.layers.normalization import BatchNormalization

#We need this to shuffle the row values to make the validation sets
import random
import math
#Remember lists are not deep copied and can be effected by replacement
import copy

#So librosa just converts the song int a numpy array. So we can use the numpy
#array to pass
class build_NN:
    def __init__(self, \
                 testing_order = 'list_validation.txt', \
                 training_location = './genres/', \
                 testing_location = './rename/', \
                 training_name = 'trainingNN_raw', \
                 testing_name = 'testingNN_raw', \
                 training_y_name = 'training_raw_y', \
                 feat_method = 'fft',\
                 normalize = 'L1',
                 val_run = True, \
                 k_folds = 3, \
                 coef_size = 1024, \
                 mfcc_ceps = 128,
                 hop_size = 509,
                 win_size = 509,
                 epochs = 26,
                 set_PCA = 0,
                 con_mat = 0):

        self.testing_order = testing_order
        self.training_location = training_location
        self.testing_location = testing_location
        self.training_name = training_name
        self.testing_name = testing_name
        self.training_y_name = training_y_name
        self.feat_method = feat_method
        self.normalize = normalize
        self.k_folds = k_folds
        self.coef_size = coef_size
        self.epochs = epochs
        self.NN_type = NN_type

        #files_location here is the top level folder where the files are located
        self.val_run = val_run

        #load_all_data passes a numpy array with every record/row being the fft
        #of the signal

        self.class_dict = self.build_classify_struct(self.training_location)

        self.confusion_matrix = np.zeros((len(self.class_dict.keys()),len(self.class_dict.keys())))

        training,result,testing = self.make_save_strings()

        if self.feat_method == "stft":
            if self.val_run:
                self.song_array_name = 'training' + '_' + 'song_array' + '.txt'
            else:
                self.song_array_name = 'testing' + '_' + 'song_array' + '.txt'

        try:
            self.training_set = np.load(training + '.npy')
            self.training_y = np.load(result + '.npy')
            if (self.feat_method == "stft") and self.val_run:
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
        except IOError:
            self.training_set, self.training_y = self.load_all_data(self.training_location)
            np.save(training,self.training_set)
            np.save(result, self.training_y)

        with open('all_training_songs.txt', "rb") as ats:
            self.all_songs = pickle.load(ats)

        with open(self.testing_order) as f:
            lines = f.readlines()
        self.lines = [line.rstrip('\n') for line in open(self.testing_order)]

        try:
            self.testing_set = np.load(testing + '.npy')
            if self.feat_method == "stft" and (not self.val_run):
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
            #print(self.testing_set.shape)
        except IOError:
            #Here we load the ordered list of the validation files into a list
            #Open the validation text file to get the correct order
            self.testing_set = self.load_testing_data(self.testing_location)
            np.save(testing, self.testing_set)

        if set_PCA:
            pca = PCA(n_components=900,svd_solver='randomized')
            pca.fit(self.training_set)
            self.training_set = pca.transform(self.training_set)
            pca2 = PCA(n_components=100,svd_solver='randomized')
            pca2.fit(self.testing_set)
            self.testing_set = pca2.transform(self.testing_set)

        #print(np.shape(self.training_y))
        #For a kaggle run we want to use the full data set with no validation
        #the if here does that
        if not self.val_run:
            y_predict = self.build_and_run_1D_CNN(self.training_set,self.training_y,self.testing_set)

            if self.feat_method == "stft":
                y_predict = self.predict_class(y_predict)
                y_predict = self.segmented_song_predict(self.lines,y_predict,self.song_array)

            #np.set_printoptions(threshold=np.nan)

            self.print_results(y_predict)

        else:
            #This function calculate the k-folds that are necessary to build
            #the validation matrices on every step both text and result
            self.build_k_fold_validation(self.training_set,self.training_y)

            accuracy_vector = []

            #After building our validation partition we have to rebuild the text
            #matrix minus one partition.
            for part in range(0,self.k_folds):
                print("The current partition being tested is partition " + str(part) + ".")
                #For this round do the following:

                #Build the validation training set
                temp_train_list = copy.deepcopy(self.val_comp_list)
                del temp_train_list[part]

                #Build the validtion training result set
                temp_result_list = copy.deepcopy(self.res_comp_list)
                del temp_result_list[part]

                #Build the validation testing result set
                temp_testing_result_set = copy.deepcopy(self.res_comp_list[part])

                #print(temp_testing_result_set)

                #Build the validation testing set
                temp_testing_set = copy.deepcopy(self.val_comp_list[part])

                #We constantly rebuild the full training set by stacking the set
                #of sparse matrices
                temp_training_set = np.vstack(temp_train_list)
                temp_result_set = np.vstack(temp_result_list)

                temp_y_predict = self.build_and_run_1D_CNN(temp_training_set, temp_result_set, temp_testing_set)

                temp_y_predict = self.predict_class(temp_y_predict)

                if self.feat_method == "stft":
                    temp_song_array = copy.deepcopy(self.song_partition[part])
                    temp_y_predict = self.segmented_song_predict(self.all_songs,temp_y_predict,temp_song_array)
                    temp_testing_result_set = self.segmented_song_predict(self.all_songs,temp_testing_result_set,temp_song_array)

                accuracy_output = self.validation_testing_accuracy(temp_y_predict,temp_testing_result_set)

                print("The accuracy for this partition is: ", accuracy_output)

                accuracy_vector.append(accuracy_output)

            print(accuracy_vector)
            self.con_mat = 1
            self.validation_testing_accuracy(temp_y_predict,temp_testing_result_set)

    def build_classify_struct(self, files_location):
        filename = os.listdir(files_location)
        class_list = []
        for genre in filename:
            class_list.append(genre)
        class_list.sort()
        class_dict = {}
        class_len = [i for i in range(0, len(class_list))]
        for class_element, j in zip(class_list, range(len(class_len))):
            class_dict[class_element] = class_len[j]
        return class_dict


    def load_all_data(self,files_location):
        coef_for_all_songs = np.empty([0,self.coef_size])
        true_genre = np.empty([0,1])
        song_array = []
        all_songs = []
        filename = os.listdir(files_location)
        for genre in filename:
            folder_path = files_location + genre
            print(folder_path)
            class_genre = self.class_dict[genre]
            for songname in os.listdir(folder_path):
                if songname.endswith(".au"):
                    all_songs.append(songname)
                    song_path = files_location + genre + "/" + songname
                    y, sr = librosa.load(song_path)
                    if self.feat_method == "fft":
                        if self.normalize == 'L1':
                            new_row = np.fft.fft(y,n=self.coef_size,norm = "ortho")
                        else:
                            new_row = np.fft.fft(y,n=self.coef_size)
                            #Weird issue here, a 1-D numpy array has no concept of a
                            #second dimension and thus cannot be transposed. This adds
                            #the second dimension for the concatenation
                        new_row = np.absolute(np.array([new_row]))
                        true_genre = np.append(true_genre, class_genre)
                        coef_for_all_songs = np.append(coef_for_all_songs, new_row, axis=0)
                    elif self.feat_method == "mfcc":
                        if self.normalize == 'L1':
                            new_row = librosa.feature.mfcc(y=y, sr=sr, norm="ortho",
                                                        n_mfcc=self.mfcc_ceps,
                                                        n_fft=self.coef_size)
                        else:
                            new_row = librosa.feature.mfcc(y=y, sr=sr,
                                                        n_mfcc=self.mfcc_ceps,
                                                        n_fft=self.coef_size)
                        #print(new_row.shape)
                        new_row_mean = np.array([np.mean(new_row, axis=0)])
                        #print(new_row_mean.shape)
                        new_row = new_row_mean[:,:self.coef_size]
                        #print(new_row.shape)
                        true_genre = np.append(true_genre, class_genre)
                        coef_for_all_songs = np.append(coef_for_all_songs, new_row, axis=0)

                    elif self.feat_method == "stft":
                        new_row = librosa.stft(y=y, win_length = self.win_size,
                                                    hop_length = self.hop_size,
                                                    n_fft = self.coef_size*2-2)
                        new_row = new_row.T
                        coef_for_all_songs = np.append(coef_for_all_songs, new_row, axis=0)

                        truth_array = np.ones([new_row.shape[0],1])*class_genre
                        true_genre = np.append(true_genre, truth_array)

                        new_array = []
                        for i in range(0, new_row.shape[0]):
                            new_array.append(songname)

                        song_array = song_array + new_array

        #print(len(song_array))
        if self.feat_method == "stft":
            song_array_name = 'training' + '_' + 'song_array' + '.txt'
            with open(song_array_name, "wb") as fp:                pickle.dump(song_array, fp)
            if self.val_run:
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
            with open('all_training_songs.txt', "wb") as ad:
                pickle.dump(all_songs, ad)

        true_genre = np.array([true_genre]).T
        print(np.shape(coef_for_all_songs))
        print(np.shape(true_genre))
        return coef_for_all_songs, true_genre


    def load_testing_data(self, files_location):
        test_coef_for_all_songs = np.empty([0,self.coef_size])
        song_array = []
        for songname in self.lines:
            if songname.endswith(".au"):
                song_path = files_location + "/" + songname
                y, sr = librosa.load(song_path)
                if self.feat_method == "fft":
                    if self.normalize == 'L1':
                        new_row = np.fft.fft(y,n=self.coef_size,norm = "ortho")
                        #Weird issue here, a 1-D numpy array has no concept of a
                        #second dimension and thus cannot be transposed. This adds
                        #the second dimension for the concatenation
                    else:
                        new_row = np.fft.fft(y,n=self.coef_size)
                    new_row = np.absolute(np.array([new_row]))
                    test_coef_for_all_songs = np.append(test_coef_for_all_songs,new_row, axis=0)

                elif self.feat_method == "mfcc":
                    new_row = librosa.feature.mfcc(y=y, sr=sr,
                                                    norm="ortho",
                                                    n_mfcc=self.mfcc_ceps,
                                                    n_fft=self.coef_size)
                    #print(new_row.shape)
                    new_row_mean = np.array([np.mean(new_row, axis=0)])
                    #print(new_row_mean.shape)
                    new_row = new_row_mean[:,:self.coef_size]
                    #print(new_row.shape)
                    test_coef_for_all_songs = np.append(test_coef_for_all_songs,new_row, axis=0)

                elif self.feat_method == "stft":
                    new_row = librosa.stft(y=y, win_length = self.win_size,
                                                    hop_length = self.hop_size,
                                                    n_fft = self.coef_size*2-2)
                    new_row = new_row.T
                    test_coef_for_all_songs = np.append(test_coef_for_all_songs, new_row, axis=0)
                    new_array = []
                    for i in range(0, new_row.shape[0]):
                        new_array.append(songname)

                    song_array = song_array + new_array
                    #print(len(song_array))

        if self.feat_method == "stft":
            song_array_name = 'testing' + '_' + 'song_array' + '.txt'
            with open(song_array_name, "wb") as fp:
                pickle.dump(song_array, fp)
            if (not self.val_run):
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)

        #print(len(song_array))
        #print(np.shape(test_coef_for_all_songs))
        return test_coef_for_all_songs

    def build_and_run_1D_CNN(self,x_train,y_train,x_test):
        y_train = keras.utils.to_categorical(y_train, num_classes=10)

        model = Sequential()
        model.add(Conv1D(64, 2, strides=(2, 2), activation="elu",
                                input_shape=(128, 128, 1)))
        model.add(MaxPooling1D(2))

        model.add(Conv1D(128, 2, strides=(2, 2), activation="elu"))
        model.add(MaxPooling1D(2))

        model.add(Conv1D(256, 2, 2, strides=(2, 2), activation="elu"))
        model.add(MaxPooling1D(2))

        model.add(Conv1D(512, 2, 2, strides=(2, 2), activation="elu"))
        model.add(MaxPooling1D(2))

        model.add(Flatten())
        model.add(Dense(1024, activation='elu', input_dim=self.coef_size))
        model.add(Dropout(0.5))

        model.add(Dense(10, activation='softmax'))

        rms = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        model.compile(loss='categorical_crossentropy',
                      optimizer=rms,
                      metrics=['accuracy'])

        model.fit(x_train, y_train, batch_size=32, nb_epoch=1)

        y_predict = model.predict(x_test)


    def print_results(self,y_predict):
        file = open('testing_results.csv','w')
        file.write("id,class\n")

        list_it = self.class_dict.items()

        list_keys = list(self.class_dict.keys())
        list_values = list(self.class_dict.values())

        for iteration in range(0,np.shape(y_predict)[0]):
            current_index = list_values.index(int(y_predict[iteration,0]))
            current_class = list_keys[current_index]

            file.write(self.lines[iteration] + ',' + current_class + '\n')

    def predict_class(self,y_predict):
        sing_vec_predict = np.empty([np.shape(y_predict)[0],1])

        for iteration in range(0,np.shape(y_predict)[0]):
            sing_vec_predict[iteration,0] = np.argmax(y_predict[iteration,:])

        return sing_vec_predict

    def segmented_song_predict(self,result_song_list,y_predict_pc,what_song):
        total_vote = np.empty((0,len(self.class_dict.keys())))
        for i in range(0,len(result_song_list)):
            result_vote = np.zeros((1,len(self.class_dict.keys())))
            flag = False
            for j in range(0, len(what_song)):
                if result_song_list[i] == what_song[j]:
                    result_vote[0,int(y_predict_pc[j])] = result_vote[0,int(y_predict_pc[j])] + 1
                    if (flag == False):
                        flag = True
            if flag == True:
                total_vote = np.append(total_vote,result_vote, axis=0)

        #np.set_printoptions(threshold=np.nan)
        #print(total_vote)

        total_vote = self.predict_class(total_vote)
        #print(total_vote)

        return total_vote

    def build_k_fold_validation(self,training_matrix,result_matrix):
        #We assume the user wants a "leave one partition out" k_folds
        #validation here

        #This is going to be a list of pointers to the matricies
        #specified by the kfolds cross validiation
        self.val_comp_list = []
        self.res_comp_list = []

        if self.feat_method == "stft":
            self.song_partition = []

        #Make a list of the row numbers for the text matrix.
        row_num_list = [j for j in range(0,training_matrix.shape[0])]

        #Then we shuffle the row numbers to make a randomization of the
        #row numbers. This is done inplace...so no need to assign the output.
        random.shuffle(row_num_list)

        #Now that we have randomly shuffled the row numbers we are going to
        #take all the n mod(k) = 0 and put it in partition 0. We will do the
        #same for 1,2, up to k-1. That will give us our k, random, partitions.

        k_fold_rem = training_matrix.shape[0] % self.k_folds
        j = math.floor(training_matrix.shape[0]/self.k_folds)
        total_rows_done = 0

        for row_mod in range(0,self.k_folds):
            #We want to balance our valdiation as much as possible so we see
            #what the remainder is from the division by k-folds. Our first j
            #matrices we build will have 1 extra row over the
            #floor(rows_text_matrix/k_folds).

            #Which side of the excess from the remainder are we on right now?
            if row_mod < k_fold_rem:
                s = j + 1
            else:
                s = j

            #Build the validation matrix partition
            new_val_matrix = np.empty([s,training_matrix.shape[1]])
            new_res_matrix = np.empty([s,1])

            #Now we are going to store the shuffled rows in the matrix
            new_val_matrix = training_matrix[row_num_list[total_rows_done:total_rows_done + s],:]
            new_res_matrix = result_matrix[row_num_list[total_rows_done:total_rows_done + s],:]

            if self.feat_method == "stft":
                new_song_matrix = [self.song_array[i] for i in row_num_list[total_rows_done:total_rows_done + s]]

            total_rows_done = total_rows_done + s

            #We build a list of the folds anad their result equivalents so we can
            #scroll through them later.
            self.val_comp_list.append(new_val_matrix)
            self.res_comp_list.append(new_res_matrix)

            if self.feat_method == "stft":
                self.song_partition.append(new_song_matrix)


    def create_heatmap(self,con_matrix, matrix_name):
        plt.figure(figsize=(12, 8))
        plt.title("Confusion Matrix for "+str(matrix_name))
        plt.xticks([i for i in range(0,len(con_matrix))])
        plt.yticks([j for j in range(0,len(con_matrix))])
        plt.xlabel("Predicted Classes")
        plt.ylabel("Actual Classes")
        plt.imshow(con_matrix, cmap='coolwarm', interpolation='nearest')
        for (j,i),label in np.ndenumerate(con_matrix):
            plt.text(i,j,label,ha='center',va='center',size=8)
        plt.show()

    def validation_testing_accuracy(self, predict_y, result_y):
        #This function gives the accuracy of the method as well as computes the
        #confusion matrix and dif_vector
        #if self.feat_method == "stft":
        #    y_predict = self.segmented_song_predict(,predict_y)

        accuracy_divider = predict_y.shape[0]
        dif_vector = (predict_y - result_y)
        num_bad_results = np.count_nonzero(dif_vector)
        #This is where the k-folds accuracy is computered
        accuracy = (accuracy_divider -  num_bad_results)/accuracy_divider

        #The confusion matrix is calculated here
        if self.con_mat:
            for i in range(0,result_y.shape[0]):
                self.confusion_matrix[int(predict_y[i,0]),int(result_y[i,0])] = \
                self.confusion_matrix[int(predict_y[i,0]),int(result_y[i,0])] + 1
            self.create_heatmap(self.confusion_matrix, "Fully Connected Neural Network")
        #print("The accuracy of this validation set was " + str(self.accuracy[-1]) + ".")
        return accuracy

        def make_save_strings(self):
        if self.feat_method == 'fft':
            training = self.feat_method + '_' + self.training_name + '_' + str(self.coef_size) + '_' + self.normalize
            result = self.feat_method + '_' + self.training_y_name + '_' + str(self.coef_size) + '_' + self.normalize
            testing = self.feat_method + '_' + self.testing_name + '_' + str(self.coef_size) + '_' + self.normalize

        elif self.feat_method == 'mfcc':
            training = self.feat_method + '_' + self.training_name + '_' + str(self.coef_size) + '_' + str(self.mfcc_ceps) + '_' + self.normalize
            result = self.feat_method + '_' + self.training_y_name + '_' + str(self.coef_size) + '_' + str(self.mfcc_ceps) + self.normalize
            testing = self.feat_method + '_' + self.testing_name + '_' + str(self.coef_size) + '_' + str(self.mfcc_ceps) + '_' + self.normalize

        elif self.feat_method == 'stft':
            training = self.feat_method + '_' + self.training_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)
            result = self.feat_method + '_' + self.training_y_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)
            testing = self.feat_method + '_' + self.testing_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)

        return training,result,testing

if __name__ == '__main__':
        new_class = build_NN(val_run = False, k_folds = 3, coef_size = 131072, epochs = 62)
        #new_class = build_NN()

