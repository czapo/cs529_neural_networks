import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import librosa


x, fs = librosa.load('blues.00000.au')

print(np.shape(x))
print(np.shape(fs))

f, t, Sxx = signal.spectrogram(x, fs)
print(np.shape(Sxx))
plt.pcolormesh(t, f, Sxx)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()

plt.specgram(x, Fs=fs, NFFT=1024)
plt.title("Spectrogram with matplotlib.pylot")
plt.show()
