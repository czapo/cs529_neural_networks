# CS_529_Neural_Networks

This project implements a Neural Network that classifies the genre of song 
snippets (each is 30 seconds) into one of several categories.

There are two files FSFC.py and PS2C.py.

FSFC stands for Full Song Fully Connected
PS2C stands for Partial Song 2D  Convolution

Important: Make sure you clean out the folder before running a new batch of
stft classifications with different parameters.

These files are to be placed into the upper level folder with the folders genres
 and rename. The files have a load feature which, for the training set, searches
the folder genres and for the testing set searches the folder rename.

For the feat_method variable you can run fft, mfcc, or stft.

An example of usage for FSFC is:
if __name__ == '__main__':
        new_class = build_NN(val_run = True, \
                             k_folds = 3, \
                             win_size = 254, \
                             hop_size = 450, \
                             coef_size = 128, \
                             epochs = 90, \
                             normalize = 'NN', \
                             set_PCA = 0, \
                             feat_method = 'stft', \
                             decor = False, \
                             NN_type = 'DNN', \
                             batch_size = 128)

This will create several files including a number of song files (to keep track
of what songs were used and the structure of songs that goes with the training
data). Note that the stft builds segments so we have to keep track of which
segments

The file FSFC.py has a description of all possible variables for the init call.

In this case the file will run a validation run, with 3 folds. The STFT will
build a vector every 450 samples, from a windows that is 254 samples wide, and 
with a vector of 128 fourier coefficients. The classifier will run for 90 epochs
. The vector will not be normalized. The PCA will not be run. The feature method
is 'stft' which stands for short time fourier transform. We do not want to 
decorrelate the frequencies. The NN_type is DNN (each file only has one type),
and the batch size for the DNN is 128.

An example of usage for PS2C is:
if __name__ == '__main__':
        new_class = build_NN(val_run = True, \
                             k_folds = 3, \
                             win_size = 254, \
                             hop_size = 225, \
                             coef_size = 128, \
                             epochs = 120, \
                             normalize = 'NN', \
                             set_PCA = 0, \
                             feat_method = 'cstft', \
                             decor = False, \
                             NN_type = 'CNN', \
                             batch_size = 128)

This will create several files including a number of song files (to keep track
of what songs were used and the structure of songs that goes with the training
data). Note that the stft builds segments so we have to keep track of which
segments

The file PS2C.py has a description of all possible variables for the init call.

In this case the file will run a validation run, with 3 folds. The STFT will
build a vector every 450 samples, from a windows that is 254 samples wide, and 
with a vector of 128 fourier coefficients. The classifier will run for 120 epochs
. The vector will not be normalized. The PCA will not be run. The feature method
is 'stft' which stands for short time fourier transform. We do not want to 
decorrelate the frequencies. The NN_type is CNN (each file only has one type),
and the batch size for the DNN is 128.

An example of the an FFT run:
if __name__ == '__main__':
        new_class = build_NN(val_run = True, \
                             k_folds = 3, \
                             coef_size = 131072, \
                             epochs = 50, \
                             normalize = 'L1', \
                             feat_method = 'fft', \
                             NN_type = 'DNN', \
                             batch_size = 128 ,\
                             DO = 0.5)

DO just stands for dropout.

