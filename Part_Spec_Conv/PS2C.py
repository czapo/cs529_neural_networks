#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Christopher Zapotocky and Francisco Viramontes
haar filter
"""
#These modules are for building the data
import os
import librosa
import numpy as np
import time

import matplotlib.pyplot as plt

#These modules are for running the Neural Network
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, MaxPooling2D, Flatten, Conv2D
from keras.optimizers import SGD, RMSprop
from keras import regularizers
from keras.layers.normalization import BatchNormalization

#We need this to shuffle the row values to make the validation sets
import random
import math
#Remember lists are not deep copied and can be effected by replacement
import copy

#For finding best components.
from sklearn.preprocessing import normalize

#this is for the song array
import pickle

#So librosa just converts the song int a numpy array. So we can use the numpy
#array to pass
class build_NN:
    def __init__(self, \
                 testing_order = 'list_validation.txt', \
                 training_location = './genres/', \
                 testing_location = './rename/', \
                 training_name = 'trainingNN_raw', \
                 testing_name = 'testingNN_raw', \
                 training_y_name = 'training_raw_y', \
                 feat_method = 'fft',\
                 normalize = 'L1', \
                 val_run = True, \
                 k_folds = 3, \
                 coef_size = 256, \
                 mfcc_ceps = 128, \
                 hop_size = 509, \
                 win_size = 1018, \
                 epochs = 26, \
                 set_PCA = 0, \
                 con_mat = 0, \
                 decor = False, \
                 NN_type = 'CNN',\
                 batch_size = 128,\
                 DO = 0.5):

        #testing_order in the name of the file where the list of the files for
        #testing are
        self.testing_order = testing_order
        #training_location is the high level folder where the folders of music
        #genre are located
        self.training_location = training_location
        #testing_location is the high level folder where the independent testing
        #examples are located
        self.testing_location = testing_location
        #training_name is the name that will be given to the training file that
        #is the raw data pulled from the .au files
        self.training_name = training_name
        #testing_name is the name that will be given to the testing file that
        #is the raw data pulled from the .au files        
        self.testing_name = testing_name
        #training_y_name is the result vector for the processed training data
        self.training_y_name = training_y_name
        #feat_method is the feature method, currently fft, mfcc, or stft used
        #for mapping the data from the time domain to the frequency domain
        self.feat_method = feat_method
        #normalize simply asks whether we want to normalize the data or not
        #this is pertainent in the fft and mfcc implementations
        self.normalize = normalize
        #k_folds is the number of folds we would like for cross-validation
        self.k_folds = k_folds
        #the coef_size is the number of fft coefficients or stft coefficients
        self.coef_size = coef_size
        #mfcc_ceps is the just the number of mfcc ceps we would like to have
        self.mfcc_ceps = mfcc_ceps
        #hop_size is the size of distance till we start looking for coefficients
        #in a new window
        self.hop_size = hop_size
        #window_size is the set of samples to include in the fft coefficient
        #calculation
        self.win_size = win_size
        #epochs is the number of epochs to run for the test
        self.epochs = epochs
        #con_mat is a just a flag that says the confusion matrix should be
        #implemented. This is done only on that last k_fold validation.
        self.con_mat = con_mat
        #decor attempts to implement decorrelation on the data
        self.decor = decor
        #NN_type just chooses Neural Network type
        self.NN_type = NN_type
        #self.batch_size chooses the batch size for training
        self.batch_size = batch_size
        #I keep dropout the same so this covers that
        self.DO = DO

        #files_location here is the top level folder where the files are located
        self.val_run = val_run

        #This builds the list of data samples that is the keys 1,2,3,... and the
        #values blues,jazz, etc...that go together.
        self.class_dict = self.build_classify_struct(self.training_location)

        #We initialize the confusion matrix size here
        self.confusion_matrix = np.zeros((len(self.class_dict.keys()),len(self.class_dict.keys())))

        #Here we call the function that makes the names for the save and load files.
        #Due to the number of parameters these have to be made for every set of
        #parameters.
        training,result,testing = self.make_save_strings()

        #This if statement chooses whether we are going to load the song list
        #for the training set or for the testing set
        if self.feat_method == "cstft":
            if self.val_run:
                self.song_array_name = 'training' + '_' + 'song_array' + '.txt'
            else:
                self.song_array_name = 'testing' + '_' + 'song_array' + '.txt'
                
        #load_all_data passes a numpy array with every record/row being the fft
        #of the signal

        #here we attempt to load data sets of data. If any piece of a data set
        #is missing we run the functions go build a data set. This first
        #statement is for the training set.
        try:
            self.training_set = np.load(training + '.npy')
            self.training_y = np.load(result + '.npy')
            if (self.feat_method == "cstft") and self.val_run:
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
        except IOError:
            self.training_set, self.training_y = self.load_all_data(self.training_location)
            np.save(training,self.training_set)
            np.save(result, self.training_y)

        #This loads the training set for all the songs in the training set
        with open('all_training_songs.txt', "rb") as ats:
            self.all_songs = pickle.load(ats)

        #self.lines is the list of all testing files to be considered
        with open(self.testing_order) as f:
            lines = f.readlines()
        self.lines = [line.rstrip('\n') for line in open(self.testing_order)]

        #the second try statement just loads the testing data from the file that
        #it was saved in before. If that the files needed for loading do not
        #exist then we make them.
        try:
            self.testing_set = np.load(testing + '.npy')
            if self.feat_method == "cstft" and (not self.val_run):
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
        except IOError:
            #Here we load the ordered list of the validation files into a list
            #Open the validation text file to get the correct order
            self.testing_set = self.load_testing_data(self.testing_location)
            np.save(testing, self.testing_set)

        #For a kaggle run we want to use the full data set with no validation
        #the if here does that.
        if not self.val_run:
            if self.NN_type == 'CNN':
                y_predict = self.build_and_run_CNN(self.training_set, self.training_y, self.testing_set)     

            #For the stft we basically did data augmentation. To get back the
            #original data set we have to vote on the results of the agumentation.
            if self.feat_method == "cstft":
                y_predict = self.predict_class(y_predict)
                y_predict = self.segmented_song_predict(self.lines,y_predict,self.song_array)

            #np.set_printoptions(threshold=np.nan)
            self.print_results(y_predict)

        #This else is for running the validation on the training set.
        else:
            #This function calculate the k-folds that are necessary to build
            #the validation matrices on every step both text and result
            self.build_k_fold_validation(self.training_set,self.training_y)

            accuracy_vector = []

            #After building our validation partition we have to rebuild the text
            #matrix minus one partition.
            for part in range(0,self.k_folds):
                print("The current partition being tested is partition " + str(part) + ".")
                #For this round do the following:

                #Build the validation training set
                temp_train_list = copy.deepcopy(self.val_comp_list)
                del temp_train_list[part]

                #Build the validtion training result set
                temp_result_list = copy.deepcopy(self.res_comp_list)
                del temp_result_list[part]

                #Build the validation testing result set
                temp_testing_result_set = copy.deepcopy(self.res_comp_list[part])

                #print(temp_testing_result_set)

                #Build the validation testing set
                temp_testing_set = copy.deepcopy(self.val_comp_list[part])

                #We constantly rebuild the full training set by stacking the set
                #of sparse matrices
                temp_training_set = np.vstack(temp_train_list)
                temp_result_set = np.vstack(temp_result_list)

                #Here we choose which neural network to implement on the current run.
                if self.NN_type == 'CNN':
                    temp_y_predict = self.build_and_run_CNN(temp_training_set, temp_result_set, temp_testing_set, temp_testing_result_set)                    

                temp_y_predict = self.predict_class(temp_y_predict)

                segment_output = self.validation_testing_accuracy(temp_y_predict,temp_testing_result_set)

                print("The accuracy for the segments of this partition is: ", segment_output)

                #For the stft we basically did data augmentation. To get back the
                #original data set we have to vote on the results of the agumentation.
                if self.feat_method == "cstft":
                    temp_song_array = copy.deepcopy(self.song_partition[part])           
                    temp_y_predict = self.segmented_song_predict(self.all_songs,temp_y_predict,temp_song_array)
                    temp_testing_result_set = self.segmented_song_predict(self.all_songs,temp_testing_result_set,temp_song_array)

                #Here we get the accuracy of the entire run.
                accuracy_output = self.validation_testing_accuracy(temp_y_predict,temp_testing_result_set)

                print("The accuracy for this partition is: ", accuracy_output)

                #We append our accuracy for the given k_fold
                accuracy_vector.append(accuracy_output)

            print(accuracy_vector)

            #This runs the confusion matrix
            self.con_mat = 1
            self.validation_testing_accuracy(temp_y_predict,temp_testing_result_set)

    def build_classify_struct(self, files_location):
        '''
        Description:
        This function takes the file location and builds a dictionary out of the
        classes. We need this go bo back and forth between the the class alias
        a number and the class string associated with the genre.

        Input:
        self -> global variables
        files_location -> string
        
        Output:
        class_dict -> dictionary
        '''
        filename = os.listdir(files_location)
        class_list = []
        for genre in filename:
            class_list.append(genre)
        class_list.sort()
        class_dict = {}
        class_len = [i for i in range(0, len(class_list))]
        for class_element, j in zip(class_list, range(len(class_len))):
            class_dict[class_element] = class_len[j]
        return class_dict

    def load_all_data(self,files_location):
        '''
        Description:
        This function takes the file location and increments through all the
        folders and files below the files_location upper folder. The songs
        are open and converter into librosa. After that we put them into
        a numpy list with fft, mfcc, or stft coefficients per song
        or per segment (in the stft case). This function is for loading the
        training data.

        Input:
        self -> global variables
        files_location -> string
        
        Output:
        coef_for_all_songs -> numpy array
        true_genre -> numpy array
        '''
        #Initialize the vectors for storing the information
        coef_for_all_songs = np.empty([0,self.coef_size,self.coef_size])
        true_genre = np.empty([0,1])
        #For the segmentation we need to store the songs for every segmentation
        song_array = []
        all_songs = []
        #This lists the folder names in the directory
        filename = os.listdir(files_location)
        #here we search by genre fulder
        for genre in filename:
            folder_path = files_location + genre
            print(folder_path)
            class_genre = self.class_dict[genre]
            #Then we search by song
            for songname in os.listdir(folder_path):
                #This avoids the case where we have a folder
                if songname.endswith(".au"):
                    #Store each song name for searching through later
                    all_songs.append(songname)
                    song_path = files_location + genre + "/" + songname
                    y, sr = librosa.load(song_path)
                    #Short Time Fourier Transform
                    if self.feat_method == "cstft":                       
                        new_row = librosa.stft(y=y, win_length = self.win_size,
                                                    hop_length = self.hop_size,
                                                    n_fft = self.coef_size*2-2)
                        
                        #Important: The STFT give complex coefficients NNs can
                        #only deal with real coefficients
                        new_row = np.abs(new_row)
                        
                        #if self.normalize == 'L1':
                        #    new_row = np.floor(255*normalize(new_row, norm='max',axis=0))
                        #This was an attempt to decorallate the stft not completed                        
                        if self.decor == True:
                            new_row = np.squre(new_row)/(self.coef_size*2-2)

                        frames_to_add = math.floor(new_row.shape[1]/self.coef_size)
                        new_row = new_row[:,0:self.coef_size*frames_to_add]
                        new_row = np.array(np.split(new_row,frames_to_add,axis=1))

                        #new_row = new_row.T
                        
                        #Store the coefficents                        
                        coef_for_all_songs = np.append(coef_for_all_songs, new_row, axis=0)

                        #Store the result values for the given record
                        truth_array = np.ones([new_row.shape[0],1])*class_genre
                        true_genre = np.append(true_genre, truth_array)

                        #We need to record what song each record is a part of
                        #so later we can vote on the segment to classify the
                        #song.
                        new_array = []
                        for i in range(0, new_row.shape[0]):
                            new_array.append(songname)

                        song_array = song_array + new_array

        #Here we write the song list out to a file so it is stored for use later
        #slight bug to fix here with overwriting the song files
        if self.feat_method == "cstft":
            song_array_name = 'training' + '_' + 'song_array' + '.txt'
            with open(song_array_name, "wb") as fp:
                pickle.dump(song_array, fp)
            if self.val_run:
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)
            with open('all_training_songs.txt', "wb") as ad:
                pickle.dump(all_songs, ad)
                        
        true_genre = np.array([true_genre]).T
        print(np.shape(coef_for_all_songs))
        print(np.shape(true_genre))
        return coef_for_all_songs, true_genre


    def load_testing_data(self, files_location):
        '''
        Description:
        This function takes the file location and increments through all the
        folders and files below the files_location upper folder. The songs
        are open and converter into librosa. After that we put them into
        a numpy list with fft, mfcc, or stft coefficients per song
        or per segment (in the stft case). This function is for loading the
        testing data.

        Input:
        self -> global variables
        files_location -> string
        
        Output:
        test_coef_for_all_songs -> numpy array
        '''
        #Initialize the vectors for storing the information
        test_coef_for_all_songs = np.empty([0,self.coef_size,self.coef_size])
        song_array = []
        #Go through the list of all songs in the testing folder
        for songname in self.lines:
            if songname.endswith(".au"):
                song_path = files_location + "/" + songname
                y, sr = librosa.load(song_path)
                #Here we pick our time to frequency domain conversion
                #Short Time Fourier Transform  
                if self.feat_method == "cstft":                                              
                    new_row = librosa.stft(y=y, win_length = self.win_size,
                                                    hop_length = self.hop_size,
                                                    n_fft = self.coef_size*2-2)

                    new_row = np.abs(new_row)

                    if self.normalize == 'L1':
                        new_row = np.floor(255*normalize(new_row, norm='max',axis=0))

                    frames_to_add = math.floor(new_row.shape[1]/self.coef_size)
                    new_row = new_row[:,0:self.coef_size*frames_to_add]
                    new_row = np.array(np.split(new_row,frames_to_add,axis=1))

                    #new_row = new_row.T
                        
                    test_coef_for_all_songs = np.append(test_coef_for_all_songs, new_row, axis=0)

                    #We need to record what song each record is a part of
                    #so later we can vote on the segment to classify the
                    #song.                        
                    new_array = []
                    for i in range(0, new_row.shape[0]):
                        new_array.append(songname)

                    song_array = song_array + new_array
                    #print(len(song_array))

        #Here we write the song list out to a file so it is stored for use later
        #slight bug to fix here with overwriting the song files     
        if self.feat_method == "cstft":
            song_array_name = 'testing' + '_' + 'song_array' + '.txt'
            with open(song_array_name, "wb") as fp:
                pickle.dump(song_array, fp)
            if (not self.val_run):
                self.song_array = []
                with open(self.song_array_name, "rb") as fp:
                     self.song_array = pickle.load(fp)

        #print(len(song_array))        
        print(np.shape(test_coef_for_all_songs))
        return test_coef_for_all_songs
    
    
    def build_and_run_CNN(self,x_train,y_train,x_test,y_test = None):
        '''
        Description:
        Contains and runs the actual classifier. This is a the deep neural
        network version.
        
        Input:
        self -> global variables
        x_train,y_train,x_test,y_test=None -> numpy arrays
        
        Output:
        y_predict -> numpy array
        '''
        y_train = keras.utils.to_categorical(y_train, num_classes=len(self.class_dict.keys()))

        x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1],x_train.shape[2],1))
        x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1],x_train.shape[2],1))

        
        model = Sequential()
        model.add(Conv2D(64, kernel_size=(2, 2),
                 activation='elu',
                 input_shape=(x_train.shape[1], x_train.shape[2],1),
                 kernel_initializer='random_uniform',
                 bias_initializer='zeros'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(128, kernel_size=(2, 2), activation='elu',
                kernel_initializer='random_uniform',
                bias_initializer='zeros'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(256, kernel_size=(2, 2), activation='elu',
                kernel_initializer='random_uniform',
                bias_initializer='zeros'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(512, kernel_size=(2, 2), activation='elu',
                kernel_initializer='random_uniform',
                bias_initializer='zeros'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        
        model.add(Flatten())
        model.add(Dense(1000, activation='elu'))
        model.add(Dropout(self.DO))
        model.add(Dense(len(self.class_dict.keys()), activation='softmax'))

        #model.compile(loss=keras.losses.categorical_crossentropy,
        #              optimizer=keras.optimizers.Adam(),
        #              metrics=['accuracy'])

        rms = RMSprop(lr=0.000025, rho=0.9, epsilon=None, decay=0.000005)
        model.compile(loss='categorical_crossentropy',
                      optimizer=rms,
                      metrics=['accuracy'])
        
        #history = AccuracyHistory()

        if y_test is not None:
            y_test = keras.utils.to_categorical(y_test, num_classes=len(self.class_dict.keys()))
            model.fit(x_train, y_train,
                    epochs=self.epochs,
                    batch_size=self.batch_size,
                    verbose=1,
                    validation_data=(x_test, y_test))

        else:
            model.fit(x_train, y_train,
                    epochs=self.epochs,
                    batch_size=self.batch_size)


        y_predict = model.predict(x_test)

        return y_predict
    
    def print_results(self,y_predict):
        '''
        Description:
        This function takes the predicted values fromt he classifier and prints
        them to file.

        Input:
        self -> global variables
        y_predict -> numpy array
        
        Output:
        None
        '''
        file = open('testing_results.csv','w')
        file.write("id,class\n")

        list_it = self.class_dict.items()

        list_keys = list(self.class_dict.keys())
        list_values = list(self.class_dict.values())

        for iteration in range(0,np.shape(y_predict)[0]):
            current_index = list_values.index(int(y_predict[iteration,0]))
            current_class = list_keys[current_index]

            file.write(self.lines[iteration] + ',' + current_class + '\n')

    def predict_class(self,y_predict):
        '''
        Description:
        This function actually counts the number or the record of a value
        and and outputs a single columned numpy array to be saved or to be
        used for figuring out the accuracy.

        Input:
        self -> global variables
        y_predict -> numpy array
        
        Output:
        sing_vec_predict -> numpy array
        '''
        sing_vec_predict = np.empty([np.shape(y_predict)[0],1])

        for iteration in range(0,np.shape(y_predict)[0]):
            sing_vec_predict[iteration,0] = np.argmax(y_predict[iteration,:])

        return sing_vec_predict

    def segmented_song_predict(self,result_song_list,y_predict_pc,what_song):
        '''
        Description:
        This function puts all the segments for each song together and outputs
        the voting result, that is classification for all the songs in the
        training set.

        Input:
        self -> global variables
        y_predict_pc -> numpy array
        result_song_list -> list
        what_song -> list
        
        Output:
        total_vote -> numpy array
        '''
        total_vote = np.empty((0,len(self.class_dict.keys())))
        for i in range(0,len(result_song_list)):
            result_vote = np.zeros((1,len(self.class_dict.keys())))
            flag = False
            for j in range(0, len(what_song)):
                if result_song_list[i] == what_song[j]:
                    result_vote[0,int(y_predict_pc[j])] = result_vote[0,int(y_predict_pc[j])] + 1
                    if (flag == False):
                        flag = True
            if flag == True:            
                total_vote = np.append(total_vote,result_vote, axis=0)
                
        #np.set_printoptions(threshold=np.nan)
        #print(total_vote)

        total_vote = self.predict_class(total_vote)
        #print(total_vote)

        return total_vote

    def build_k_fold_validation(self,training_matrix,result_matrix):
        '''
        Description:
        This function splits the data and builds k_folds from the data sets.

        Input:
        self -> global variables
        training_matrix -> numpy array
        result_matrix -> numpy array
        
        Output:
        None
        '''
        #We assume the user wants a "leave one partition out" k_folds
        #validation here

        #This is going to be a list of pointers to the matricies
        #specified by the kfolds cross validiation
        self.val_comp_list = []
        self.res_comp_list = []

        if self.feat_method == "cstft":
            self.song_partition = []

        #Make a list of the row numbers for the text matrix.
        row_num_list = [j for j in range(0,training_matrix.shape[0])]

        #Then we shuffle the row numbers to make a randomization of the
        #row numbers. This is done inplace...so no need to assign the output.
        random.shuffle(row_num_list)

        #Now that we have randomly shuffled the row numbers we are going to
        #take all the n mod(k) = 0 and put it in partition 0. We will do the
        #same for 1,2, up to k-1. That will give us our k, random, partitions.

        k_fold_rem = training_matrix.shape[0] % self.k_folds
        j = math.floor(training_matrix.shape[0]/self.k_folds)
        total_rows_done = 0

        for row_mod in range(0,self.k_folds):
            #We want to balance our valdiation as much as possible so we see
            #what the remainder is from the division by k-folds. Our first j
            #matrices we build will have 1 extra row over the
            #floor(rows_text_matrix/k_folds).

            #Which side of the excess from the remainder are we on right now?
            if row_mod < k_fold_rem:
                s = j + 1
            else:
                s = j

            #Build the validation matrix partition
            new_val_matrix = np.empty([s,training_matrix.shape[1]])
            new_res_matrix = np.empty([s,1])

            #Now we are going to store the shuffled rows in the matrix
            new_val_matrix = training_matrix[row_num_list[total_rows_done:total_rows_done + s],:]
            new_res_matrix = result_matrix[row_num_list[total_rows_done:total_rows_done + s],:]

            if self.feat_method == "cstft":
                new_song_matrix = [self.song_array[i] for i in row_num_list[total_rows_done:total_rows_done + s]]

            total_rows_done = total_rows_done + s

            #We build a list of the folds anad their result equivalents so we can
            #scroll through them later.
            self.val_comp_list.append(new_val_matrix)
            self.res_comp_list.append(new_res_matrix)

            if self.feat_method == "cstft":
                self.song_partition.append(new_song_matrix)

    def create_heatmap(self,con_matrix, matrix_name):
        '''
        Description:
        This function makes the confusion matrix image for the current run in
        the validation.

        Input:
        self -> global variables
        matrix_name -> numpy array
        
        Output:
        None
        '''
        plt.figure(figsize=(12, 8))
        plt.title("Confusion Matrix for "+str(matrix_name))
        plt.xticks([i for i in range(0,len(con_matrix))])
        plt.yticks([j for j in range(0,len(con_matrix))])
        plt.xlabel("Predicted Classes")
        plt.ylabel("Actual Classes")
        plt.imshow(con_matrix, cmap='coolwarm', interpolation='nearest')
        for (j,i),label in np.ndenumerate(con_matrix):
            plt.text(i,j,label,ha='center',va='center',size=8)
        plt.show()

    def validation_testing_accuracy(self, predict_y, result_y):
        '''
        Description:
        This function comes up with the accuracy by comparing the predicted
        y value against the true y value. The confusion matrix is also created
        here when selected.
        
        Input:
        self -> global variables        
        predict_y -> numpy array
        result_y -> numpy array
        
        Output:
        accuracy -> float
        '''
        #This function gives the accuracy of the method as well as computes the
        #confusion matrix and dif_vector
        #if self.feat_method == "stft":
        #    y_predict = self.segmented_song_predict(,predict_y)

        accuracy_divider = predict_y.shape[0]
        dif_vector = (predict_y - result_y)
        num_bad_results = np.count_nonzero(dif_vector)
        #This is where the k-folds accuracy is computered
        accuracy = (accuracy_divider -  num_bad_results)/accuracy_divider

        #The confusion matrix is calculated here
        if self.con_mat:
            for i in range(0,result_y.shape[0]):
                self.confusion_matrix[int(predict_y[i,0]),int(result_y[i,0])] = \
                self.confusion_matrix[int(predict_y[i,0]),int(result_y[i,0])] + 1
            self.create_heatmap(self.confusion_matrix, "Fully Connected Neural Network")
        #print("The accuracy of this validation set was " + str(self.accuracy[-1]) + ".")
        return accuracy

    #This function gives all the names needed to save and load each feature method type.
    def make_save_strings(self):
        '''
        Description:
        This function takes the important parameters int he current run and builds
        training, result, and testing names from them.

        Input:
        self -> global variables
        
        Output:
        training -> string
        result -> string
        testing -> string
        '''
        if self.feat_method == 'cstft':
            training = self.feat_method + '_' + self.training_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)
            result = self.feat_method + '_' + self.training_y_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)
            testing = self.feat_method + '_' + self.testing_name + '_' + str(self.coef_size) + '_' + str(self.hop_size) + '_' + str(self.win_size)

        return training,result,testing

#class AccuracyHistory(keras.callbacks.Callback):
#    def on_train_begin(self, logs={}):
#        self.acc = []
#
#    def on_epoch_end(self, batch, logs={}):
#        self.acc.append(logs.get('acc'))

if __name__ == '__main__':
        new_class = build_NN(val_run = True, \
                             k_folds = 3, \
                             win_size = 254, \
                             hop_size = 225, \
                             coef_size = 128, \
                             epochs = 120, \
                             normalize = 'NN', \
                             set_PCA = 0, \
                             feat_method = 'cstft', \
                             decor = False, \
                             NN_type = 'CNN', \
                             batch_size = 128)

