(venv) mldl00@mldl00:~/cs529_neural_networks/FSFC$ python FSFC.py
Using TensorFlow backend.
The current partition being tested is partition 0.
Train on 600 samples, validate on 300 samples
Epoch 1/50
2018-11-14 02:13:25.919485: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1411] Found device 0 with properties: 
name: GeForce GTX 1070 Ti major: 6 minor: 1 memoryClockRate(GHz): 1.683
pciBusID: 0000:02:00.0
totalMemory: 7.92GiB freeMemory: 7.71GiB
2018-11-14 02:13:25.919521: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1490] Adding visible gpu devices: 0
2018-11-14 02:13:26.142442: I tensorflow/core/common_runtime/gpu/gpu_device.cc:971] Device interconnect StreamExecutor with strength 1 edge matrix:
2018-11-14 02:13:26.142485: I tensorflow/core/common_runtime/gpu/gpu_device.cc:977]      0 
2018-11-14 02:13:26.142510: I tensorflow/core/common_runtime/gpu/gpu_device.cc:990] 0:   N 
2018-11-14 02:13:26.142705: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1103] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 7434 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1070 Ti, pci bus id: 0000:02:00.0, compute capability: 6.1)
2018-11-14 02:13:26.266316: W tensorflow/core/framework/allocator.cc:113] Allocation of 536870912 exceeds 10% of system memory.
2018-11-14 02:13:26.446196: W tensorflow/core/framework/allocator.cc:113] Allocation of 536870912 exceeds 10% of system memory.
2018-11-14 02:13:26.616235: W tensorflow/core/framework/allocator.cc:113] Allocation of 536870912 exceeds 10% of system memory.
2018-11-14 02:13:26.785998: W tensorflow/core/framework/allocator.cc:113] Allocation of 536870912 exceeds 10% of system memory.
2018-11-14 02:13:26.960378: W tensorflow/core/framework/allocator.cc:113] Allocation of 536870912 exceeds 10% of system memory.
600/600 [==============================] - 5s 8ms/step - loss: 2.6039 - acc: 0.1133 - val_loss: 2.2238 - val_acc: 0.1300
Epoch 2/50
600/600 [==============================] - 1s 2ms/step - loss: 2.4048 - acc: 0.1367 - val_loss: 2.1572 - val_acc: 0.1867
Epoch 3/50
600/600 [==============================] - 1s 2ms/step - loss: 2.3426 - acc: 0.1550 - val_loss: 2.0775 - val_acc: 0.2600
Epoch 4/50
600/600 [==============================] - 1s 2ms/step - loss: 2.2411 - acc: 0.1883 - val_loss: 2.0229 - val_acc: 0.3000
Epoch 5/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1897 - acc: 0.1917 - val_loss: 1.9878 - val_acc: 0.3333
Epoch 6/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1545 - acc: 0.2350 - val_loss: 1.9317 - val_acc: 0.3800
Epoch 7/50
600/600 [==============================] - 1s 2ms/step - loss: 2.0312 - acc: 0.2550 - val_loss: 1.8980 - val_acc: 0.3633
Epoch 8/50
600/600 [==============================] - 1s 2ms/step - loss: 2.0044 - acc: 0.3050 - val_loss: 1.8674 - val_acc: 0.3600
Epoch 9/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9907 - acc: 0.2833 - val_loss: 1.8151 - val_acc: 0.4033
Epoch 10/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9290 - acc: 0.3000 - val_loss: 1.7778 - val_acc: 0.4067
Epoch 11/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8712 - acc: 0.3500 - val_loss: 1.7507 - val_acc: 0.4400
Epoch 12/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8423 - acc: 0.3550 - val_loss: 1.7177 - val_acc: 0.4300
Epoch 13/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7382 - acc: 0.3983 - val_loss: 1.7020 - val_acc: 0.4200
Epoch 14/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7194 - acc: 0.3817 - val_loss: 1.6805 - val_acc: 0.4133
Epoch 15/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6885 - acc: 0.4033 - val_loss: 1.6297 - val_acc: 0.4733
Epoch 16/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6659 - acc: 0.4283 - val_loss: 1.6239 - val_acc: 0.4467
Epoch 17/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5955 - acc: 0.4483 - val_loss: 1.6119 - val_acc: 0.4367
Epoch 18/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5397 - acc: 0.4517 - val_loss: 1.5647 - val_acc: 0.4667
Epoch 19/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5108 - acc: 0.5100 - val_loss: 1.5488 - val_acc: 0.4633
Epoch 20/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4349 - acc: 0.4983 - val_loss: 1.5614 - val_acc: 0.4667
Epoch 21/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4718 - acc: 0.4983 - val_loss: 1.5477 - val_acc: 0.4700
Epoch 22/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4215 - acc: 0.4967 - val_loss: 1.5233 - val_acc: 0.4667
Epoch 23/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3795 - acc: 0.5217 - val_loss: 1.5090 - val_acc: 0.4733
Epoch 24/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3405 - acc: 0.5200 - val_loss: 1.4944 - val_acc: 0.4733
Epoch 25/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2815 - acc: 0.5467 - val_loss: 1.4852 - val_acc: 0.4833
Epoch 26/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2554 - acc: 0.5567 - val_loss: 1.4848 - val_acc: 0.4867
Epoch 27/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2174 - acc: 0.5750 - val_loss: 1.4864 - val_acc: 0.4733
Epoch 28/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0979 - acc: 0.6367 - val_loss: 1.4659 - val_acc: 0.4933
Epoch 29/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1465 - acc: 0.6100 - val_loss: 1.4722 - val_acc: 0.4767
Epoch 30/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0817 - acc: 0.6417 - val_loss: 1.4784 - val_acc: 0.4800
Epoch 31/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0625 - acc: 0.6367 - val_loss: 1.4719 - val_acc: 0.4833
Epoch 32/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9787 - acc: 0.6833 - val_loss: 1.4765 - val_acc: 0.4933
Epoch 33/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9885 - acc: 0.6667 - val_loss: 1.4749 - val_acc: 0.4967
Epoch 34/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9697 - acc: 0.6867 - val_loss: 1.4948 - val_acc: 0.4900
Epoch 35/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9520 - acc: 0.6817 - val_loss: 1.4528 - val_acc: 0.5100
Epoch 36/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9170 - acc: 0.6950 - val_loss: 1.5024 - val_acc: 0.5000
Epoch 37/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8219 - acc: 0.7400 - val_loss: 1.4670 - val_acc: 0.5033
Epoch 38/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7850 - acc: 0.7417 - val_loss: 1.5191 - val_acc: 0.4900
Epoch 39/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8061 - acc: 0.7200 - val_loss: 1.4965 - val_acc: 0.5067
Epoch 40/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7659 - acc: 0.7500 - val_loss: 1.4928 - val_acc: 0.5067
Epoch 41/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7419 - acc: 0.7467 - val_loss: 1.4927 - val_acc: 0.5267
Epoch 42/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6980 - acc: 0.7583 - val_loss: 1.5195 - val_acc: 0.5133
Epoch 43/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6459 - acc: 0.7933 - val_loss: 1.5238 - val_acc: 0.5067
Epoch 44/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6486 - acc: 0.7933 - val_loss: 1.5839 - val_acc: 0.5033
Epoch 45/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5872 - acc: 0.8317 - val_loss: 1.5340 - val_acc: 0.5167
Epoch 46/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5729 - acc: 0.8233 - val_loss: 1.5647 - val_acc: 0.5067
Epoch 47/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5348 - acc: 0.8450 - val_loss: 1.5803 - val_acc: 0.5133
Epoch 48/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5607 - acc: 0.8200 - val_loss: 1.6288 - val_acc: 0.4933
Epoch 49/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5862 - acc: 0.8217 - val_loss: 1.5460 - val_acc: 0.5100
Epoch 50/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5107 - acc: 0.8600 - val_loss: 1.5843 - val_acc: 0.5100
The accuracy for the segments of this partition is:  0.51
The accuracy for this partition is:  0.51
The current partition being tested is partition 1.
Train on 600 samples, validate on 300 samples
Epoch 1/50
600/600 [==============================] - 4s 7ms/step - loss: 2.6094 - acc: 0.0933 - val_loss: 2.2317 - val_acc: 0.1200
Epoch 2/50
600/600 [==============================] - 1s 2ms/step - loss: 2.4506 - acc: 0.1450 - val_loss: 2.1649 - val_acc: 0.1900
Epoch 3/50
600/600 [==============================] - 1s 2ms/step - loss: 2.3723 - acc: 0.1517 - val_loss: 2.1135 - val_acc: 0.2267
Epoch 4/50
600/600 [==============================] - 1s 2ms/step - loss: 2.2387 - acc: 0.1850 - val_loss: 2.0678 - val_acc: 0.2667
Epoch 5/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1845 - acc: 0.2217 - val_loss: 2.0101 - val_acc: 0.3100
Epoch 6/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1856 - acc: 0.2000 - val_loss: 1.9647 - val_acc: 0.3533
Epoch 7/50
600/600 [==============================] - 1s 2ms/step - loss: 2.0835 - acc: 0.2583 - val_loss: 1.9357 - val_acc: 0.3867
Epoch 8/50
600/600 [==============================] - 1s 2ms/step - loss: 2.0020 - acc: 0.2767 - val_loss: 1.8838 - val_acc: 0.3933
Epoch 9/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9712 - acc: 0.3117 - val_loss: 1.8378 - val_acc: 0.4033
Epoch 10/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9248 - acc: 0.3367 - val_loss: 1.8046 - val_acc: 0.3933
Epoch 11/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8588 - acc: 0.3783 - val_loss: 1.7634 - val_acc: 0.4067
Epoch 12/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8570 - acc: 0.3417 - val_loss: 1.7471 - val_acc: 0.4033
Epoch 13/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8016 - acc: 0.3817 - val_loss: 1.7036 - val_acc: 0.4300
Epoch 14/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7232 - acc: 0.3950 - val_loss: 1.6644 - val_acc: 0.4433
Epoch 15/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7136 - acc: 0.4200 - val_loss: 1.6533 - val_acc: 0.4567
Epoch 16/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7174 - acc: 0.4483 - val_loss: 1.6267 - val_acc: 0.4433
Epoch 17/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6175 - acc: 0.4417 - val_loss: 1.5936 - val_acc: 0.4333
Epoch 18/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5446 - acc: 0.4600 - val_loss: 1.5718 - val_acc: 0.4733
Epoch 19/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5458 - acc: 0.4717 - val_loss: 1.5489 - val_acc: 0.4767
Epoch 20/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4924 - acc: 0.4883 - val_loss: 1.5430 - val_acc: 0.4600
Epoch 21/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4473 - acc: 0.4800 - val_loss: 1.5214 - val_acc: 0.4500
Epoch 22/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4013 - acc: 0.5017 - val_loss: 1.5080 - val_acc: 0.4733
Epoch 23/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3602 - acc: 0.5517 - val_loss: 1.4863 - val_acc: 0.4800
Epoch 24/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3459 - acc: 0.5550 - val_loss: 1.4839 - val_acc: 0.4633
Epoch 25/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2852 - acc: 0.5500 - val_loss: 1.4960 - val_acc: 0.4967
Epoch 26/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2374 - acc: 0.5883 - val_loss: 1.4424 - val_acc: 0.5000
Epoch 27/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1714 - acc: 0.6250 - val_loss: 1.4164 - val_acc: 0.5133
Epoch 28/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1962 - acc: 0.5933 - val_loss: 1.4147 - val_acc: 0.5133
Epoch 29/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1334 - acc: 0.6283 - val_loss: 1.4331 - val_acc: 0.5100
Epoch 30/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0800 - acc: 0.6433 - val_loss: 1.3979 - val_acc: 0.5233
Epoch 31/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1001 - acc: 0.6333 - val_loss: 1.4514 - val_acc: 0.5000
Epoch 32/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0443 - acc: 0.6733 - val_loss: 1.3963 - val_acc: 0.5333
Epoch 33/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9853 - acc: 0.6700 - val_loss: 1.3951 - val_acc: 0.5033
Epoch 34/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9883 - acc: 0.6833 - val_loss: 1.4395 - val_acc: 0.5200
Epoch 35/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9350 - acc: 0.6967 - val_loss: 1.3655 - val_acc: 0.5400
Epoch 36/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9150 - acc: 0.7067 - val_loss: 1.3915 - val_acc: 0.5167
Epoch 37/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8598 - acc: 0.6933 - val_loss: 1.4048 - val_acc: 0.5367
Epoch 38/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8468 - acc: 0.7117 - val_loss: 1.3622 - val_acc: 0.5433
Epoch 39/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8188 - acc: 0.7250 - val_loss: 1.4348 - val_acc: 0.5200
Epoch 40/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7959 - acc: 0.7117 - val_loss: 1.3820 - val_acc: 0.5367
Epoch 41/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7259 - acc: 0.7750 - val_loss: 1.3961 - val_acc: 0.5300
Epoch 42/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7948 - acc: 0.7567 - val_loss: 1.4067 - val_acc: 0.5433
Epoch 43/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6880 - acc: 0.7883 - val_loss: 1.3878 - val_acc: 0.5500
Epoch 44/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6910 - acc: 0.7650 - val_loss: 1.3925 - val_acc: 0.5500
Epoch 45/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6324 - acc: 0.7800 - val_loss: 1.3972 - val_acc: 0.5267
Epoch 46/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6126 - acc: 0.8067 - val_loss: 1.4044 - val_acc: 0.5533
Epoch 47/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5764 - acc: 0.8250 - val_loss: 1.4369 - val_acc: 0.5333
Epoch 48/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5757 - acc: 0.8200 - val_loss: 1.4314 - val_acc: 0.5300
Epoch 49/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5632 - acc: 0.8250 - val_loss: 1.4335 - val_acc: 0.5500
Epoch 50/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5616 - acc: 0.8383 - val_loss: 1.4257 - val_acc: 0.5433
The accuracy for the segments of this partition is:  0.5433333333333333
The accuracy for this partition is:  0.5433333333333333
The current partition being tested is partition 2.
Train on 600 samples, validate on 300 samples
Epoch 1/50
600/600 [==============================] - 4s 7ms/step - loss: 2.6093 - acc: 0.1200 - val_loss: 2.2548 - val_acc: 0.1267
Epoch 2/50
600/600 [==============================] - 1s 2ms/step - loss: 2.4409 - acc: 0.1267 - val_loss: 2.1810 - val_acc: 0.2367
Epoch 3/50
600/600 [==============================] - 1s 2ms/step - loss: 2.3818 - acc: 0.1450 - val_loss: 2.1154 - val_acc: 0.3033
Epoch 4/50
600/600 [==============================] - 1s 2ms/step - loss: 2.3319 - acc: 0.1650 - val_loss: 2.0703 - val_acc: 0.3833
Epoch 5/50
600/600 [==============================] - 1s 2ms/step - loss: 2.2471 - acc: 0.2000 - val_loss: 2.0198 - val_acc: 0.3700
Epoch 6/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1895 - acc: 0.2267 - val_loss: 1.9723 - val_acc: 0.4033
Epoch 7/50
600/600 [==============================] - 1s 2ms/step - loss: 2.1654 - acc: 0.2233 - val_loss: 1.9363 - val_acc: 0.4200
Epoch 8/50
600/600 [==============================] - 1s 2ms/step - loss: 2.0094 - acc: 0.3100 - val_loss: 1.9006 - val_acc: 0.4200
Epoch 9/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9895 - acc: 0.2983 - val_loss: 1.8492 - val_acc: 0.4433
Epoch 10/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9472 - acc: 0.3350 - val_loss: 1.8114 - val_acc: 0.4400
Epoch 11/50
600/600 [==============================] - 1s 2ms/step - loss: 1.9508 - acc: 0.3183 - val_loss: 1.7791 - val_acc: 0.4500
Epoch 12/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8524 - acc: 0.3850 - val_loss: 1.7523 - val_acc: 0.4633
Epoch 13/50
600/600 [==============================] - 1s 2ms/step - loss: 1.8493 - acc: 0.3683 - val_loss: 1.7077 - val_acc: 0.4633
Epoch 14/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7873 - acc: 0.3783 - val_loss: 1.6875 - val_acc: 0.4800
Epoch 15/50
600/600 [==============================] - 1s 2ms/step - loss: 1.7098 - acc: 0.4133 - val_loss: 1.6612 - val_acc: 0.4733
Epoch 16/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6396 - acc: 0.4533 - val_loss: 1.6122 - val_acc: 0.4767
Epoch 17/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6474 - acc: 0.4183 - val_loss: 1.6131 - val_acc: 0.4667
Epoch 18/50
600/600 [==============================] - 1s 2ms/step - loss: 1.6201 - acc: 0.4500 - val_loss: 1.5903 - val_acc: 0.4767
Epoch 19/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5242 - acc: 0.4833 - val_loss: 1.5758 - val_acc: 0.4767
Epoch 20/50
600/600 [==============================] - 1s 2ms/step - loss: 1.5187 - acc: 0.4950 - val_loss: 1.5423 - val_acc: 0.4933
Epoch 21/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4407 - acc: 0.5317 - val_loss: 1.5334 - val_acc: 0.4967
Epoch 22/50
600/600 [==============================] - 1s 2ms/step - loss: 1.4052 - acc: 0.5250 - val_loss: 1.5116 - val_acc: 0.4933
Epoch 23/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3785 - acc: 0.5433 - val_loss: 1.4988 - val_acc: 0.5133
Epoch 24/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3325 - acc: 0.5533 - val_loss: 1.4759 - val_acc: 0.5233
Epoch 25/50
600/600 [==============================] - 1s 2ms/step - loss: 1.3129 - acc: 0.5417 - val_loss: 1.4516 - val_acc: 0.5200
Epoch 26/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2398 - acc: 0.6000 - val_loss: 1.4646 - val_acc: 0.5133
Epoch 27/50
600/600 [==============================] - 1s 2ms/step - loss: 1.2072 - acc: 0.5750 - val_loss: 1.4712 - val_acc: 0.5300
Epoch 28/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1860 - acc: 0.5983 - val_loss: 1.4427 - val_acc: 0.5433
Epoch 29/50
600/600 [==============================] - 1s 2ms/step - loss: 1.1307 - acc: 0.6367 - val_loss: 1.4642 - val_acc: 0.5367
Epoch 30/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0903 - acc: 0.6350 - val_loss: 1.4755 - val_acc: 0.5233
Epoch 31/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0811 - acc: 0.6317 - val_loss: 1.4770 - val_acc: 0.5133
Epoch 32/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0155 - acc: 0.6733 - val_loss: 1.4654 - val_acc: 0.5367
Epoch 33/50
600/600 [==============================] - 1s 2ms/step - loss: 1.0123 - acc: 0.6750 - val_loss: 1.4815 - val_acc: 0.5200
Epoch 34/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9937 - acc: 0.6783 - val_loss: 1.4594 - val_acc: 0.5300
Epoch 35/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9394 - acc: 0.7133 - val_loss: 1.4354 - val_acc: 0.5467
Epoch 36/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8967 - acc: 0.6983 - val_loss: 1.4758 - val_acc: 0.5467
Epoch 37/50
600/600 [==============================] - 1s 2ms/step - loss: 0.9036 - acc: 0.7050 - val_loss: 1.4637 - val_acc: 0.5467
Epoch 38/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8416 - acc: 0.7317 - val_loss: 1.4623 - val_acc: 0.5467
Epoch 39/50
600/600 [==============================] - 1s 2ms/step - loss: 0.8043 - acc: 0.7567 - val_loss: 1.4440 - val_acc: 0.5600
Epoch 40/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7748 - acc: 0.7600 - val_loss: 1.4839 - val_acc: 0.5367
Epoch 41/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7452 - acc: 0.7717 - val_loss: 1.5027 - val_acc: 0.5467
Epoch 42/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7570 - acc: 0.7450 - val_loss: 1.5185 - val_acc: 0.5500
Epoch 43/50
600/600 [==============================] - 1s 2ms/step - loss: 0.7524 - acc: 0.7467 - val_loss: 1.5280 - val_acc: 0.5267
Epoch 44/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6848 - acc: 0.7950 - val_loss: 1.5190 - val_acc: 0.5567
Epoch 45/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6418 - acc: 0.7867 - val_loss: 1.5206 - val_acc: 0.5533
Epoch 46/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6933 - acc: 0.7800 - val_loss: 1.5331 - val_acc: 0.5367
Epoch 47/50
600/600 [==============================] - 1s 2ms/step - loss: 0.6211 - acc: 0.7983 - val_loss: 1.4865 - val_acc: 0.5700
Epoch 48/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5947 - acc: 0.8033 - val_loss: 1.5512 - val_acc: 0.5433
Epoch 49/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5785 - acc: 0.8017 - val_loss: 1.5252 - val_acc: 0.5633
Epoch 50/50
600/600 [==============================] - 1s 2ms/step - loss: 0.5688 - acc: 0.8150 - val_loss: 1.5786 - val_acc: 0.5533
The accuracy for the segments of this partition is:  0.5533333333333333
The accuracy for this partition is:  0.5533333333333333
[0.51, 0.5433333333333333, 0.5533333333333333]
